import { useState } from "react";
import { Button } from "@syncu-softwares/sd-react";

function Example() {
  const [count, setCount] = useState(0);
  return (
    <>
      <Button onClick={() => setCount((prev) => prev + 1)}>Count : {count}</Button>
    </>
  );
}

export default Example;
