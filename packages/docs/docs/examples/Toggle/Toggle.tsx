import { useState } from "react";
import { Toggle } from "@syncu-softwares/sd-react";
import React from "react";

function Example() {
  const [checked, setChecked] = useState(true);
  return (
    <>
      <Toggle
        isChecked={checked}
        onChange={(value) => setChecked(value)}
        stateText={true}
        onLabel="On"
        offLabel="Off"
      />
      <br />
      Toggle State : {checked ? "On" : "Off"}
    </>
  );
}

export default Example;
