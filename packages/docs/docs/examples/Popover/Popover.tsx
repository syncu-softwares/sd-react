import React from "react";
import { Popover } from "@syncu-softwares/sd-react";

function PopoverExample() {
  return (
    <>
      <Popover
        trigger="mouseenter focusin"
        content={<>Im a Tooltip</>}
        placement="auto"
      >
        <span>Hover me</span>
      </Popover>

      <Popover
        trigger="click"
        content={<>Im a Popover</>}
        placement="auto"
      >
        <span>Click me</span>
      </Popover>
    </>
  );
}

export default PopoverExample;
