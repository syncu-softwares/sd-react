import * as React from 'react';
import * as SDReact from '@syncu-softwares/sd-react';

// Add react-live imports you need here
const ReactLiveScope = {
  React,
  ...React,
  ...SDReact,
};

delete (ReactLiveScope as any).default;

export default ReactLiveScope;
