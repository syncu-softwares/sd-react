import { defineConfig } from "vite";
import { resolve } from "path";

export default defineConfig({
  resolve: {
    alias: {
      "components": resolve(__dirname, "src/components"),
      "utils": resolve(__dirname, "src/utils"),
    },
  },
});
