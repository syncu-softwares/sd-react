import React, {
  ChangeEvent,
  createContext,
  ReactElement,
  ReactNode,
  useContext,
  useMemo,
} from "react";
import Label from "../Label/Label";
import { get, set } from "lodash";

type FormContextType<T = Record<string, unknown>> = {
  value: T;
  onChange: (data: T) => void;
};

const FormContext = createContext<FormContextType<any> | undefined>(undefined);

type FormProps<T> = FormContextType<T> & {
  children: ReactNode;
};

function Form<T>(props: FormProps<T>) {
  const { children, ...contextValue } = props;
  return (
    <FormContext.Provider value={contextValue}>{children}</FormContext.Provider>
  );
}

type FormItemProps = {
  name: string;
  label?: string;
  children: ReactElement;
  valuePropName?: string;
};

function FormItem({
  name,
  label,
  children,
  valuePropName = "value",
}: FormItemProps) {
  const context = useContext(FormContext);
  if (!context) {
    throw new Error("<Form.item> must be child of a <Form> element");
  }

  const id = useMemo(
    () => `form_${name}_${Math.random().toString(36).slice(2)}`,
    [name]
  );

  const { value, onChange } = context;
  const cloneChild = React.cloneElement(children, {
    id,
    [valuePropName]: children.props[valuePropName] ?? get(value, name),
    onChange: (e: unknown, v: unknown) => {
      const changedVal = v ?? e;
      const val = value ?? {};
      switch (typeof changedVal) {
        case "string":
        case "number":
        case "boolean":
        case "undefined":
          set(val, name, changedVal);
          break;
        case "object": {
          if (changedVal instanceof CustomEvent) {
            switch (changedVal.type) {
              case "checkboxClicked":
                set(val, name, changedVal.detail.isChecked);
                break;
              case "radioClicked":
                set(val, name, changedVal.detail.uid);
                break;
            }
          } else if (changedVal?.hasOwnProperty("target")) {
            set(
              val,
              name,
              (changedVal as ChangeEvent<HTMLInputElement>).target.value
            );
          } else {
            set(val, name, changedVal);
          }
          break;
        }
      }
      onChange({ ...val });
    },
  });
  return (
    <div>
      {label && <Label htmlFor={id}>{label}</Label>}
      {cloneChild}
    </div>
  );
}

Form.Item = FormItem;
export default Form;
