import React, { AriaAttributes } from "react";
import PropTypes from "prop-types";
import Icon from "../Icon/Icon";
import { sizes } from "utils/constants";
import "./Label.scss";

export type LabelProps = {
  children?: any;
  icon?: string;
  iconSize?: "sm" | "md" | "lg" | "xl" | undefined;
  className?: string;
  customClass?: string;
  tabIndex?: number;
  ariaAttributes?: AriaAttributes;
  htmlFor?: string;
  iconAtEnd?: boolean;
};

function Label({
  children,
  icon,
  iconSize,
  className,
  customClass,
  tabIndex,
  ariaAttributes,
  htmlFor,
  iconAtEnd,
  ...props
}: LabelProps) {
  return (
    <label
      {...props}
      className={`sd-label ${className ?? ""} ${customClass ?? ""}`}
      tabIndex={tabIndex}
      {...ariaAttributes}
      htmlFor={htmlFor}
    >
      {!iconAtEnd && icon && <Icon name={icon} size={iconSize}></Icon>}
      {children}
      {iconAtEnd && icon && <Icon name={icon} size={iconSize}></Icon>}
    </label>
  );
};

Label.propTypes = {
  children: PropTypes.node,
  icon: PropTypes.string,
  iconSize: PropTypes.oneOf(sizes),
  className: PropTypes.string,
  customClass: PropTypes.string,
  tabIndex: PropTypes.number,
  ariaAttributes: PropTypes.any,
  htmlFor: PropTypes.string,
};

export default Label;
