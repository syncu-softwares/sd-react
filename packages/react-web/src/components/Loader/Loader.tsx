import React from "react";
import "./Loader.scss";

export type LoaderProps = {
  size?: "xs" | "sm" | "md" | "lg";
  className?: string;
  isInline?: boolean;
  isFullPage?: boolean;
  children?: any;
  isHorizontallyCentered?: boolean;
};

export default function Loader(props: LoaderProps) {
  const {
    className = "",
    size = "md",
    children,
    isFullPage,
    isHorizontallyCentered,
  } = props;
  const spinner = <div className={`loading-spinner loading-spinner__${size}`}>{children}</div>;

  if (isFullPage) {
    return <div className={className + " full-page-loader"}>{spinner}</div>;
  } else if (isHorizontallyCentered) {
    return (
      <div className={className + " horizontally-centered"}>{spinner}</div>
    );
  }
  return spinner;
};

