import React, {
  ChangeEvent,
  InputHTMLAttributes,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { KEY_ENTER, EVENT_TYPE_CLICK } from "utils/KeyConstants";
import styles from "./Input.module.scss";
import Icon from "components/Icon/Icon";
import Label from "components/Label/Label";

type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  /**
   * Label for input
   */
  label?: string;
  /**
   * Size of the input
   */
  size?: "sm" | "md" | "lg";
  /**
   * Makes Label and input inside same row
   */
  inline?: boolean;
  /**
   * Makes input with text overflow ellipsis 
   */
  truncate?: boolean;
  /**
   * Shows Clear icon input
   */
  clearable?: boolean;
  /**
   * Shows error in input 
   */
  error?: boolean;
  /**
   * Error Message in input
   */
  errorMessage?: string | string[];
  testId?: string;
  /**
   * Value to input 
   */
  value?: string | number | undefined;
  /**
   * onChange callback for input
   * @param e Change Event object
   * @param value current change value
   * @returns void
   */
  onChange?: (e?: ChangeEvent, value?: any) => void;
};

const Input = forwardRef(function Input(props: InputProps, ref: any) {
  const {
    label,
    size = "md",
    inline,
    truncate,
    clearable,
    value: initValue,
    error,
    errorMessage,
    type = "text",
    disabled = false,
    maxLength,
    testId,
    className,
    onChange,
    onClick,
    ...restProps
  } = props;
  const [value, setValue] = useState(initValue);
  const [inputType, setInputType] = useState(type);

  useEffect(() => {
    setValue(initValue);
  }, [initValue]);

  useImperativeHandle(
    ref,
    () => ({
      clear: clearValue,
      ...ref,
    }),
    []
  );

  const onEdit = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e?.target.value);
    if (onChange) {
      onChange(e);
    }
  };

  const onInputClick = (e: any) => {
    if (type != "file" && type != "date") {
      e.preventDefault();
    }
    if (onClick) {
      onClick(e);
    }
  };

  const clearValue = (e: any) => {
    if (e.key === KEY_ENTER || e.key === EVENT_TYPE_CLICK) {
      e.preventDefault();
      setValue("");
      onChange?.(undefined, "");
    }
  };

  const showPasswordValue = () => {
    !disabled && setInputType(inputType === "text" ? "password" : "text");
  };

  return (
    <div className={className}>
      <div
        className={
          inline ? `${styles.inputwrapper} ${styles["wrapper-inline"]}` : ""
        }
      >
        {label && <Label>{label}</Label>}
        <div className={styles.inputBox}>
          <input
            ref={ref}
            {...restProps}
            type={inputType}
            value={initValue ?? value}
            onChange={onEdit}
            onClick={onInputClick}
            maxLength={maxLength}
            className={`sd-input ${truncate ? styles["truncated-input"] : ""}
            ${styles[`${size}-input`]} ${styles.baseinput} ${error ? styles["error-input"] : ""} ${
              disabled ? styles["input-disabled"] : ""
            }`}
            disabled={disabled}
            data-testid={testId}
          />
          {clearable && !error && !!value && !disabled && (
            <Icon
              name="fa-regular fa-circle-xmark"
              onClick={clearValue}
              tabIndex={-1}
              custom-class={`${styles["clear-icon"]} ${styles[`${size}-icon`]}`}
            />
          )}
          {type === "password" && !error && (
            <Icon
              custom-class={`${styles["clear-icon"]} ${styles[`${size}-icon`]} ${
                disabled ? styles["input-disabled"] : ""
              }`}
              name={
                inputType === "password"
                  ? "fa-solid fa-eye"
                  : "fa-solid fa-eye-slash"
              }
              onClick={showPasswordValue}
              tabIndex={-1}
            />
          )}
          {error && (
            <Icon
              customClass={`${styles["clear-icon"]} ${styles[`${size}-icon`]}
                 ${styles["error-icon"]} ${styles[`error-icon-${type}`]}`}
              name="fa-regular fa-circle-exclamation"
              tabIndex={-1}
            />
          )}
          {errorMessage && (
            <div className={styles["error-message"]}>
              {typeof errorMessage == "string"
                ? errorMessage
                : errorMessage.map((e, i) => <div key={i}>{e}</div>)}
            </div>
          )}
        </div>
      </div>
    </div>
  );
});

Input.displayName = "Input";
export default Input;
