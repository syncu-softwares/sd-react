import React, { AriaAttributes, useRef, useState } from "react";
import "./Tooltip.scss";
import { forEach } from "lodash";
import Tippy from "@tippyjs/react";

export interface TooltipProps {
  arrow?: boolean;
  headerText?: any;
  customClass?: string;
  className?: string;
  placement?:
    | "auto"
    | "auto-start"
    | "auto-end"
    | "top"
    | "bottom"
    | "right"
    | "left"
    | "top-start"
    | "top-end"
    | "bottom-start"
    | "bottom-end"
    | "right-start"
    | "right-end"
    | "left-start"
    | "left-end";
  trigger?: any;
  width?: any;
  distance?: any;
  interactive?: boolean;
  duration?: [number, number];
  content: any;
  tabIndex?: any;
  skidding?: any;
  children: any;
  ariaAttributes?: AriaAttributes;
  contentCustomClass?: any;
  appendTo?: any;
  showTooltipOnOverflow?: boolean;
  onTooltipCreate?: any;
  onTooltipDestroy?: any;
  onTriggerClick?: any;
  onContentClick?: any;
  onTooltipShow?: any;
  onTooltipHide?: any;
  contentDisplayWidth?: string;
}

export default function Tooltip(props: TooltipProps) {
  const {
    placement = "auto",
    trigger = "mouseenter focusin",
    skidding = 0,
    distance = 8,
    duration = [300, 0],
    interactive = true,
    customClass = "",
    className = "",
    headerText = "",
    content,
    children,
    tabIndex,
    ariaAttributes,
    arrow = true,
    contentCustomClass = "",
    appendTo = "parent",
    showTooltipOnOverflow = false,
    onTooltipCreate,
    onTooltipDestroy,
    onTriggerClick,
    onContentClick,
    onTooltipShow,
    onTooltipHide,
  } = props;

  const role = "tooltip";
  const [tooltipInstance, setTooltipInstance] = useState<any>();
  const tooltipRef = useRef(null);
  const tooltipContentRef = useRef(null);
  const onContentOver = () => {
    const element: any = tooltipContentRef.current;
    if (element) {
      let maxScrollWidth = 0;
      forEach(element.children, (elm) => {
        maxScrollWidth =
          elm.scrollWidth > maxScrollWidth ? elm.scrollWidth : maxScrollWidth;
      });
      const scrollWidth =
        element.children && element.children.length > 0
          ? maxScrollWidth
          : element.scrollWidth;
      if (element && tooltipInstance) {
        if (element.offsetWidth >= scrollWidth && showTooltipOnOverflow) {
          tooltipInstance.disable();
        } else {
          if (scrollWidth > element.offsetWidth) {
            tooltipInstance.enable();
            tooltipInstance.show();
          }
        }
      }
    }
  };

  const handleOnCreate = (instance: any) => {
    setTooltipInstance(instance);
    onTooltipCreate?.(instance);
  };

  const handleOnDestroy = (instance: any) => {
    onTooltipDestroy?.(instance);
  };

  const handleOnShow = (instance: any) => {
    onTooltipShow?.(instance);
  };

  const handleOnHide = (instance: any) => {
    onTooltipHide?.(instance);
  };

  const handleOnTriggerClick = (instance: any, event: any) => {
    onTriggerClick?.(instance, event);
  };

  const handleOnContentClick = (event: any) => {
    onContentClick && onContentClick(event);
  };

  const handleOnClickOutside = (instance: any, event: any) => {
    if (!event) return;
    const { popper } = instance;
    const path = event.composedPath();
    instance.__actuallyClickedInside = false;
    // this means click happened inside the popover
    if (path.some((element: any) => element === popper)) {
      // tippy adds click listener on document, and listeners om
      // document give "target" as the actual <custom-element> tag,
      // so in case if the target had shadowRoot property, let's
      // trigger click on first element of event.path

      if (event.target?.shadowRoot) {
        // any other better way of checking? customElements.get(event.target.localName) ?
        let preventImplicitClickViaClassname = false;

        path.every((element: any) => {
          if (element === popper || preventImplicitClickViaClassname) {
            //break loop if popper element is encountered or class name is found
            return false;
          }
          // add this class to any particular element that is triggering unnecessary extra click
          preventImplicitClickViaClassname = element?.classList?.contains(
            "popover-prevent-implicit-click"
          );
          return true;
        });

        if (!preventImplicitClickViaClassname) {
          // this implicit click is needed at most places but creates two click issue on few places
          // so kept it in condition to take control
          path[0].click();
        }
        instance.__actuallyClickedInside = true;
      }
    }
  };

  return (
    <div className={`sd-tooltip ${customClass} ${className}`} ref={tooltipRef}>
      <div
        className="sd-tooltip__trigger"
        tabIndex={tabIndex}
        {...ariaAttributes}
      >
        <Tippy
          trigger={trigger}
          arrow={arrow}
          interactive={interactive}
          duration={duration}
          placement={placement}
          role={role}
          offset={[skidding, distance]}
          appendTo={appendTo}
          onCreate={handleOnCreate}
          onDestroy={handleOnDestroy}
          onShow={handleOnShow}
          onHide={handleOnHide}
          onTrigger={handleOnTriggerClick}
          onClickOutside={handleOnClickOutside}
          content={
            <div
              role="tooltip"
              id="tooltipContent"
              className={`sd-tooltip__content ${contentCustomClass}`}
              onClick={handleOnContentClick}
            >
              {headerText && (
                <div className="sd-tooltip__header">
                  {headerText && (
                    <div className="sd-tooltip__header-text">{headerText}</div>
                  )}
                </div>
              )}
              {content}
            </div>
          }
        >
          <div
            className="sd-tooltip__trigger__content"
            aria-describedby="tooltipContent"
            ref={tooltipContentRef}
            onMouseOver={onContentOver}
            {...ariaAttributes}
          >
            {children}
          </div>
        </Tippy>
      </div>
    </div>
  );
}
