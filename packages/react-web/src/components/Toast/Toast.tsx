import Box from "../ShadowBox/ShadowBox";
import React, { useEffect } from "react";
import styles from "./Toast.module.scss";

export type ToastProps = {
  toast?: { type?: string; message?: string };
  close?: () => void;
  duration?: number;
};

export default function Toast({ toast, duration, close }: ToastProps) {
  useEffect(() => {
    setTimeout(() => {
      close?.();
    }, duration ?? 3000);
  }, [toast]);

  return (
    <>
      {toast && toast.type && toast.message && (
        <Box className={`${styles.showToast} ${styles[toast.type]}`}>
          <div className={styles.header}></div>
          <div className={styles.body}>{toast.message}</div>
        </Box>
      )}
    </>
  );
}
