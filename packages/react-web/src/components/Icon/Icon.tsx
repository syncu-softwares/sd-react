import React, { ReactNode } from "react";
import PropTypes from "prop-types";
import "./Icons.scss";

export type IconProps = {
  name: string | ReactNode;
  size?: "sm" | "md" | "lg" | "xl" | undefined;
  onClick?: (event?: any) => void;
  onKeyDown?: (event?: any) => void;
  customClass?: string;
  tabIndex?: number;
};

function Icon({
  name,
  size = "md",
  customClass = "",
  tabIndex = 0,
  onClick,
  ...props
}: IconProps) {
  const onKeyPress = (e: any) => {};

  if (typeof name === "string") {
    return name.indexOf(".") !== -1 ? (
      <img
        className={customClass}
        tabIndex={tabIndex}
        onClick={(evt) => onClick?.(evt)}
        onKeyDown={onKeyPress}
        src={name}
        alt={name}
        {...props}
      />
    ) : (
      <i
        tabIndex={tabIndex}
        onClick={(evt) => onClick?.(evt)}
        onKeyDown={onKeyPress}
        className={`${name} ${customClass}`}
        {...props}
      />
    );
  } else if (typeof name === "object") {
    return (
      <div
        className={`sd-icon sd-icon--${size} ${customClass}`}
        tabIndex={tabIndex}
        onClick={(evt) => onClick?.(evt)}
        onKeyDown={onKeyPress}
      >
        {name}
      </div>
    );
  } else {
    return null;
  }
}

Icon.propTypes = {
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  size: PropTypes.string,
  onClick: PropTypes.func,
  onKeyDown: PropTypes.func,
  customClass: PropTypes.string,
  tabIndex: PropTypes.number,
};
export default Icon;
