import React from "react";
import styles from "./Row.module.scss";

const JUSTIFY_MAP: Partial<Record<NonNullable<RowProps["justify"]>, string>> = {
  start: "flex-start",
  end: "flex-end",
  between: "space-between",
  around: "space-around",
  evenly: "space-evenly",
};

const ALIGN_MAP: Partial<Record<NonNullable<RowProps["align"]>, string>> = {
  start: "flex-start",
  end: "flex-end",
};

export type RowProps = {
  justify?: "start" | "end" | "center" | "between" | "around" | "evenly";
  align?: "start" | "end" | "center" | "stretch" | "baseline";
  wrap?: boolean;
} & Pick<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
  "children" | "className" | "aria-label" | "style" | "tabIndex"
>;

export default function Row({
  children,
  justify,
  align,
  wrap,
  className,
  style,
  ...divprops
}: RowProps &
  React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  >) {
  return (
    <div
      className={`${styles.row} ${wrap ? styles.wrap : ""} ${className}`}
      style={{
        ...style,
        justifyContent: justify && (JUSTIFY_MAP[justify] ?? justify),
        alignItems: align && (ALIGN_MAP[align] ?? align),
      }}
      {...divprops}
    >
      {children}
    </div>
  );
}
