import { TippyProps } from '@tippyjs/react'
import detectOverflow from '@popperjs/core/lib/utils/detectOverflow';

type Modifiers=NonNullable<TippyProps['popperOptions']>['modifiers'];

const calcMaxSize={
    name:'calcMaxSize',
    enabled:true,
    phase:'main',
    requireIfExists:['offset','preventOverflow','flip'],
    fn({state,name}: any){
        const overflow=detectOverflow(state);
        const {x,y}=state.modifiersData.preventOverflow ||{x:0,y:0};
        const {width,height}=state.rects.popper;
        const [basePlacement]=state.placement.split('-');

        const widthProp=basePlacement==='left'?'left':'right';
        const heightProp=basePlacement==='top'?'top':'bottom';

        state.modifiersData[name]={
            width:width-overflow[widthProp]-x-10,
            height:height-overflow[heightProp]-y-10,
        };
    },
};

const applyMaxSize={
    name:'applyMaxSize',
    enabled:true,
    phase:'write',
    requires:['calcMaxsize'],
    fn({state}: any){
        const {width,height}=state.modifiersData.calcMaxSize;

        state.element.popper.style.maxWidth=`${width}px`;
     state.element.popper.style.maxHeight=`${height}px`;
     state.element.popper.style.overflowY='auto';

    },
};

const maxSizeModifiers=[calcMaxSize,applyMaxSize,{name:'flip',enabled:false}] as Modifiers;

export default maxSizeModifiers;