import React, { AriaAttributes, useState } from "react";
import "tippy.js/dist/tippy.css";
import "tippy.js/dist/svg-arrow.css";
import "tippy.js/dist/backdrop.css";
import "tippy.js/dist/border.css";
import Tippy from "@tippyjs/react";
import { KEY_ENTER, EVENT_TYPE_CLICK, ESC_KEY_CODE } from "utils/KeyConstants";
import "./Popover.scss";
import maxSizeModifiers from "./maxSize";

export interface PopoverProps {
  content?: any;
  trigger?: string;
  arrow?: boolean;
  allowHTML?: boolean;
  customClass?: string;
  className?: string;
  distance?: any;
  interactive?: boolean;
  duration?: [number, number];
  placement?:
    | "auto"
    | "auto-start"
    | "auto-end"
    | "top"
    | "bottom"
    | "right"
    | "left"
    | "top-start"
    | "top-end"
    | "bottom-start"
    | "bottom-end"
    | "right-start"
    | "right-end"
    | "left-start"
    | "left-end";
  role?: string;
  zIndex?: number;
  skidding?: any;
  hideOnClick?: boolean;
  onPopoverCreate?: any;
  onPopoverDestroy?: any;
  onContentClick?: any;
  onPopoverHide?: any;
  onPopoverShow?: any;
  onPopoverUpdate?: any;
  onTriggerClick?: any;
  onPopoverKeyDown?: any;
  tabIndex?: number;
  ariaAttributes?: AriaAttributes;
  children?: any;
  appendTo?: any;
  preventImplicitClick?: boolean;
  maxWidth?: number | string;
  constrainToViewport?: boolean;
  delay?: number;
}

export default function Popover(props: PopoverProps) {
  const {
    role = "popover",
    arrow = true,
    placement = "auto",
    trigger = "mouseenter focusin",
    allowHTML = true,
    hideOnClick = true,
    zIndex = 9999,
    skidding = 0,
    distance = 8,
    duration = [300, 0],
    interactive = true,
    appendTo = "parent",
    tabIndex,
    ariaAttributes,
    onPopoverCreate,
    onPopoverDestroy,
    onPopoverShow,
    onPopoverHide,
    onTriggerClick,
    onPopoverKeyDown,
    onContentClick,
    customClass = "",
    className = "",
    content,
    children,
    preventImplicitClick,
    maxWidth,
    constrainToViewport,
    delay,
  } = props;

  const [popoverInstance, setPopoverInstance] = useState<any>(null);

  const handleOnCreate = (instance: any) => {
    setPopoverInstance(instance);
    onPopoverCreate && onPopoverCreate(instance);
  };

  const handleOnDestroy = (instance: any) => {
    onPopoverDestroy && onPopoverDestroy(instance);
  };

  const handleOnShow = (instance: any) => {
    onPopoverShow && onPopoverShow(instance);
  };

  const handleOnHide = (instance: any) => {
    if (onPopoverHide) {
      // you can return false to prevent hiding the tippy instance
      if (onPopoverHide(instance) === false) {
        return false;
      }
    }
    return;
  };

  const handleOnClickOutside = (instance: any, event: any) => {
    if (!event) return;
    const { popper } = instance;
    const path = event.composedPath();
    instance.__actuallyClickedInside = false;
    // this means click happened inside the popover
    if (path.some((element: any) => element === popper)) {
      // tippy adds click listener on document, and listeners om
      // document give "target" as the actual <custom-element> tag,
      // so in case if the target had shadowRoot property, let's
      // trigger click on first element of event.path

      if (event.target?.shadowRoot) {
        // any other better way of checking? customElements.get(event.target.localName) ?
        let preventImplicitClickViaClassname = false;

        path.every((element: any) => {
          if (element === popper || preventImplicitClickViaClassname) {
            //break loop if popper element is encountered or class name is found
            return false;
          }
          // add this class to any particular element that is triggering unnecessary extra click
          preventImplicitClickViaClassname = element?.classList?.contains(
            "popover-prevent-implicit-click"
          );
          return true;
        });

        if (!preventImplicitClick && !preventImplicitClickViaClassname) {
          // this implicit click is needed at most places but creates two click issue on few places
          // so kept it in condition to take control
          path[0].click();
        }
        instance.__actuallyClickedInside = true;
      }
    }
  };

  const handleOnTriggerClick = (instance: any, event: any) => {
    onTriggerClick && onTriggerClick(instance, event);
  };

  const handleOnContentClick = (event: any) => {
    onContentClick && onContentClick(event);
  };

  const handleKeyDown = (event: { key: string }) => {
    // In case of tabIndex being undefined, we can ignore the 'keyDown' event on popover to manually show it.
    if (
      tabIndex !== undefined &&
      event.key === KEY_ENTER &&
      trigger === EVENT_TYPE_CLICK
    ) {
      popoverInstance.show();
    } else if (event.key === ESC_KEY_CODE) {
      popoverInstance.hide();
    }
    onPopoverKeyDown && onPopoverKeyDown(event);
  };

  return content ? (
    <div
      className={`sd-popover ${customClass} ${className}`}
      tabIndex={tabIndex}
      onKeyDown={(e: any) => {
        handleKeyDown(e);
      }}
    >
      <Tippy
        trigger={trigger}
        arrow={arrow}
        allowHTML={allowHTML}
        interactive={interactive}
        duration={duration}
        placement={placement}
        role={role}
        offset={[skidding, distance]}
        hideOnClick={hideOnClick}
        appendTo={appendTo}
        zIndex={zIndex}
        maxWidth={maxWidth}
        onCreate={handleOnCreate}
        onDestroy={handleOnDestroy}
        onShow={handleOnShow}
        onHide={handleOnHide}
        onTrigger={handleOnTriggerClick}
        onClickOutside={handleOnClickOutside}
        animation="fade"
        delay={delay}
        popperOptions={
          constrainToViewport ? { modifiers: maxSizeModifiers } : undefined
        }
        content={
          <div className="sd-popover__content" onClick={handleOnContentClick}>
            {content}
          </div>
        }
      >
        <div className="sd-popover__trigger" {...ariaAttributes}>
          {children}
        </div>
      </Tippy>
    </div>
  ) : (
    <>{children}</>
  );
}
