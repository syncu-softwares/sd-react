import React, { ReactElement, ReactNode, forwardRef } from "react";
import styles from "./Page.module.scss";
import Row from "components/Row/Row";
import Icon from "components/Icon/Icon";
import Loader from "components/Loader/Loader";

type PageProps = Omit<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
  "title" | "css"
> & {
  actions?: ReactElement[];
  icon?: string;
  pills?: string[];
  title?: string;
  description?: string;
  isLoading?: boolean;
  backButton?: ReactNode;
  mode?: "full" | "centered";
  errorMessage?: string;
  noPadding?: boolean;
};

const Page = forwardRef<HTMLDivElement, PageProps>(
  (
    {
      className = "",
      actions,
      icon,
      pills,
      title,
      description,
      isLoading,
      backButton,
      mode = "full",
      errorMessage,
      noPadding,
      children,
      ...divProps
    },
    ref
  ) => {
    const iconElement = icon ? (
      icon.match(/\.(png|jpe?g|gif|svg)$/) ? (
        <img src={icon} alt="Icon" />
      ) : (
        <Icon name={icon} />
      )
    ) : null;

    const showPageHeader =
      title || description || backButton || actions || icon || pills;
    const pageTitle = (
      <div>
        <div>
          {iconElement}
          <div
            className={`${styles.title} ${backButton ? styles.md : styles.lg}`}
          >
            {title}
          </div>
        </div>
      </div>
    );
    return (
      <div
        {...divProps}
        className={`${styles.container} ${className}`}
        ref={ref}
      >
        <div
          className={`${styles.scrollContainer} ${styles[mode] ?? ""}`}
          style={noPadding ? { padding: 0 } : undefined}
        >
          {showPageHeader && (
            <div className={styles.header}>
              {backButton ? (
                <>
                  <Row justify="between">
                    {backButton}
                    {actions && <Row wrap>{actions}</Row>}
                  </Row>
                  <div className={styles.mt15}>{pageTitle}</div>
                </>
              ) : (
                <Row justify="between">
                  {pageTitle}
                  {actions && <Row wrap>{actions}</Row>}
                </Row>
              )}
            </div>
          )}
          {isLoading && <Loader isFullPage></Loader>}
          {errorMessage && !isLoading && (
            <Row justify="center" className={styles.error}>
              {errorMessage}
            </Row>
          )}
          {!isLoading && !errorMessage && (
            <div className={styles.content}>{children}</div>
          )}
        </div>
      </div>
    );
  }
);

export default Page;
