import React, { ReactNode, useEffect, useState } from "react";
import Button from "components/Button/Button";
import "./Modal.scss";
import { find, isFunction } from "lodash";
import { ESC_KEY_CODE } from "utils/KeyConstants";
import Loader from "components/Loader/Loader";

type ModalProps = {
  customClass?: string;
  ClassName?: string;
  isOpen?: boolean;
  ShowCloseIcon?: boolean;
  closeonEsc?: boolean;
  isLoadingState?: boolean;
  zIndex?: number;
  width?: number;
  size?: string;
  onCloseClicked?: (x: { close: boolean; error: any }) => void;
  onOpened?: (x?: boolean) => void;
  onClosed?: (x?: boolean) => void;
  icon?: ReactNode;
  title?: ReactNode;
  children?: ReactNode;
  alignFooter?: "start" | "middle" | "end";
  footer?: ReactNode;
  buttons?: any;
  onBtnClicked?: any;
};

const BTN_CANCEL = "cancel";
const BTN_CLOSE = "close";
const BTN_OK = "ok";
const BTN_DELETE = "delete";
const BTN_CONFIRM = "confirm";
const BTN_YES = "yes";
const BTN_NO = "no";

function Modal({
  customClass = "",
  ClassName = "",
  isOpen = false,
  ShowCloseIcon = true,
  closeonEsc = true,
  isLoadingState,
  zIndex = 20,
  width,
  size = "md",
  onCloseClicked,
  onOpened,
  onClosed,
  icon,
  title,
  children,
  alignFooter,
  footer,
  buttons,
  onBtnClicked,
  ...props
}: ModalProps) {
  const DEFAULT_BUTTONS = [
    {
      id: BTN_CANCEL,
      type: "hollow",
      label: "Cancel",
      isAsync: false,
    },
    {
      id: BTN_CLOSE,
      type: "hollow",
      label: "Close",
      isAsync: false,
    },
    {
      id: BTN_OK,
      type: "solid",
      label: "Ok",
      isAsync: true,
    },
    {
      id: BTN_CONFIRM,
      type: "solid",
      label: "Confirm",
      isAsync: true,
    },
    {
      id: BTN_YES,
      type: "solid",
      label: "Yes",
      isAsync: true,
    },
    {
      id: BTN_NO,
      type: "hollow",
      label: "No",
      isAsync: false,
    },
  ];

  const [modalState, setModalState] = useState<boolean>(isOpen);
  const [promiseState, setPromiseState] = useState({ isLoading: false });
  const [activeBtnId, setActiveBtnId] = useState("");

  const closeModal = (error?: any) => {
    setActiveBtnId("");
    setModalState(false);
    closeClickedHandler({ error });
  };

  const closeClickedHandler = (error?: any) => {
    onCloseClicked && onCloseClicked({ close: true, error });
  };

  const onActionBtnClicked = (button: any) => {
    if (promiseState.isLoading) {
      return;
    }
    if (!onBtnClicked) {
      closeModal();
      return;
    }
    const promise = onBtnClicked(button);
    if (onBtnClicked && promise && isFunction(promise.then)) {
      setPromiseState((state) => {
        state.isLoading = true;
        return state;
      });
      setActiveBtnId(button.id);
      promise
        .then(() => {
          setPromiseState({
            isLoading: false,
          });
          closeModal();
        })
        .catch((error: any) => {
          setPromiseState({
            isLoading: false,
          });
          closeModal(error);
        });
    } else {
      closeModal();
    }
  };

  useEffect(() => {
    modalState ? onOpened && onOpened(true) : onClosed && onClosed(true);
    const closeModal = (e: any) => {
      if (e.key === ESC_KEY_CODE && modalState && closeonEsc) {
        closeClickedHandler();
        setModalState(false);
      }
    };
    if (closeonEsc) {
      window.addEventListener("keydown", closeModal);
    }
    return () => {
      window.addEventListener("keydown", closeModal);
    };
  }, [modalState]);

  useEffect(() => {
    setModalState(isOpen);
    if (isOpen) {
      const modalContent = document.getElementsByClassName(
        "sd-modal-content"
      )[0] as HTMLElement;
      setTimeout(() => {
        if (modalContent) {
          modalContent?.focus();
        }
      }, 0);
    }
  }, [isOpen]);

  return (
    <div className={modalState ? "sd-modal__container" : ""}>
      <div
        {...props}
        className={`sd-modal ${customClass} ${ClassName} ${modalState ? "sd-modal--open" : "sd-modal--closed"
          }`}
        style={{ zIndex: zIndex }}
      >
        <div className={`sd-modal__dialog sd-modal__dialog--${size}`}>
          <section className="sd-modal__content" tabIndex={0}>
            <header className="sd-modal__header">
              {/* {icon && <div className=" sd-modal__header__icon">{icon}</div>} */}
              {title && <div className=" sd-modal__header__title">{title}</div>}
              {ShowCloseIcon && (
                <div className="sd-modal__header__close-icon">
                  <Button
                    aria-label="close"
                    type="link"
                    size="sm"
                    onClick={closeClickedHandler}
                  >
                    X{/* <Icon name="X" size="lg"></Icon> */}
                  </Button>
                </div>
              )}
            </header>

            <div
              className={`sd-modal__body ${isLoadingState ? "sd-modal__body__loading" : ""
                }`}
            >
              {children}
            </div>

            {(!!footer || !!buttons) && (
              <footer
                className={`sd-modal__footer sd-modal__footer__${alignFooter}`}
              >
                {!!footer && footer}
                {!!buttons &&
                  ((typeof buttons === "string" &&
                    buttons.split(",").map((button) => {
                      const btnObject: any = find(DEFAULT_BUTTONS, (btn) => {
                        return btn.id === button;
                      });
                      const isLoading =
                        promiseState.isLoading && activeBtnId === btnObject.id;
                      return (
                        <Button
                          key={`btn-${btnObject.id}`}
                          type={btnObject?.type}
                          onClick={() => onActionBtnClicked(button)}
                          size="md"
                        >
                          {isLoading && <Loader size="sm"></Loader>}
                          {!isLoading && btnObject?.label}
                        </Button>
                      );
                    })) ||
                    (buttons &&
                      buttons.length &&
                      buttons.map((button: any) => {
                        return (
                          <Button
                            key={`btn-${button.id}`}
                            id={button.id}
                            onClick={() => onActionBtnClicked(button)}
                            customClass={button.className}
                            size={button.size || "md"}
                          >
                            {promiseState.isLoading &&
                              activeBtnId === button.id && (
                                <Loader size="sm"></Loader>
                              )}
                            {activeBtnId !== button.id && button?.label}
                          </Button>
                        );
                      })))}
              </footer>
            )}
          </section>
        </div>
      </div>
    </div>
  );
};

export default Modal;
