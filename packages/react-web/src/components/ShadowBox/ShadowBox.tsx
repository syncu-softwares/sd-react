import React, { ReactNode } from "react";
import "./ShadowBox.scss";

type ShadowBoxProps = {
  className?: string;
  style?: Record<string, string | number>;
  children?: ReactNode;
};
export default function ShadowBox({
  className,
  style,
  children,
}: ShadowBoxProps) {
  const classes = "sd-shadowbox " + className;
  return (
    <div style={style} className={classes}>
      {children}
    </div>
  );
}
