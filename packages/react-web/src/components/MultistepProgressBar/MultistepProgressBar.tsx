import React, {
  Dispatch,
  ForwardedRef,
  forwardRef,
  SetStateAction,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import styles from "./MultistepProgressBar.module.scss";

export type progressBarRef = {
  step: number;
  setStep: Dispatch<SetStateAction<number>>;
};

type progressBarProps = {
  steps: { label: string }[];
  clickable?: boolean;
  onStepChange?: () => void;
};

export default forwardRef(function MultistepProgressBar(
  { steps, clickable, onStepChange }: progressBarProps,
  ref: ForwardedRef<progressBarRef>
) {
  const [step, setStep] = useState<number>(1);

  useEffect(() => {
    onStepChange?.();
  }, [step]);

  useImperativeHandle(
    ref,
    () => {
      return {
        step,
        setStep,
      };
    },
    [step, setStep]
  );
  return (
    <div className={`${styles.progressBar} mb-5`}>
      {steps.map((s, i) => (
        <span
          onClick={() => (clickable ? setStep(i + 1) : null)}
          className={`${clickable ? styles.clickable : ""}${
            step >= i + 1 ? styles.active : ""
          }`}
        >
          {s.label}
        </span>
      ))}
    </div>
  );
});
