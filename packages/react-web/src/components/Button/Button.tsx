import React, { ButtonHTMLAttributes } from "react";
import Loader from "components/Loader/Loader";
import "./Button.scss";

export type ButtonProps = Omit<ButtonHTMLAttributes<HTMLButtonElement>, "type"> & {
  /**
   * Size of the button
   */
  size?: "xs" | "sm" | "md" | "lg";
  /**
   * Appearance of the button
   */
  type?:
    | "solid"
    | "hollow"
    | "link"
    | "danger-solid"
    | "danger-hollow"
    | "danger-link";
  /**
   * Makes the button loading
   */
  isLoading?: boolean;
  testID?: string;
};

export default function Button({
  size = "md",
  type = "solid",
  testID,
  isLoading,
  className,
  onClick,
  disabled,
  onKeyDown,
  ...props
}: ButtonProps) {
  const disableClass = disabled ? "disabled" : "";
  return (
    <button
      onClick={(event) => onClick && !disabled && onClick(event)}
      onKeyDown={(event) => onClick && !disabled && onKeyDown?.(event)}
      className={`sd-btn ${className} sd-btn--${size} sd-btn--${type} ${disableClass}`}
      data-testid={testID}
      disabled={disabled}
      {...props}
    >
      {isLoading && <Loader size={size} isHorizontallyCentered></Loader>}
      {!isLoading && props.children}
    </button>
  );
}
