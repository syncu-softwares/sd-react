import React, { useState, useEffect, ReactNode } from "react";
import "./Switch.scss"; // Import the styles as a CSS or SCSS file

interface SwitchProps {
  /**
   * Checked state of the switch
   */
  checked?: boolean;
  /**
   * Id of the switch
   */
  id?: string;
  /**
   * Label of the switch
   */
  label?: string | ReactNode;
  /**
   * Value of the switch
   */
  value?: string | number;
  /**
   * Required property of the switch
   */
  required?: boolean;
  /**
   * Aria-labelledby property of the switch
   */
  ariaLabelledby?: string;
  /**
   * Disabled property of the switch
   */
  disabled?: boolean;
  /**
   * Reversed state of the switch
   */
  reversed?: boolean;
  /**
   * Invalid state of the switch
   */
  invalid?: true | false | null;
  /**
   * Size of the Switch
   */
  size: "sm" | "md" | "lg" | "xl";
  /**
   * Change handler
   */
  onChange?: (checked: boolean, value?: number | string) => void;
  /**
   * Update handler
   */
  onUpdateChecked?: (checked: boolean, value?: number | string) => void;
}

const Switch: React.FC<SwitchProps> = ({
  checked = false,
  id,
  label,
  value,
  required = false,
  ariaLabelledby,
  disabled = false,
  reversed = false,
  invalid = null,
  size = "md",
  onChange,
  onUpdateChecked,
}) => {
  const [internalState, setInternalState] = useState<boolean>(checked);

  // Update internal state when the 'checked' prop changes
  useEffect(() => {
    setInternalState(checked);
  }, [checked]);

  const toggleSwitch = () => {
    if (!disabled) {
      const newCheckedValue = !internalState;
      setInternalState(newCheckedValue);

      if (onChange) onChange(newCheckedValue, value);
      if (onUpdateChecked) onUpdateChecked(newCheckedValue, value);
    }
  };

  const handleKeyUp = (event: React.KeyboardEvent) => {
    if (event.key === "Enter" || event.key === " ") {
      event.preventDefault();
      toggleSwitch();
    }
  };

  const computedSwitchClass = `sd-switch ${disabled ? "disabled" : ""} ${
    reversed ? "sd-switch__reversed" : ""
  }`;

  const computedSwitchSliderClass = `sd-switch__slider sd-switch__slider--${size} ${
    invalid === true
      ? "sd-switch__slider--invalid"
      : invalid === false
        ? "sd-switch__slider--valid"
        : ""
  }`;

  return (
    <div className={computedSwitchClass}>
      <label
        className={`sd-switch__switch sd-switch__switch--${size}`}
        htmlFor={id}
      >
        <input
          id={id}
          type="checkbox"
          checked={internalState}
          value={value}
          required={required}
          disabled={disabled}
          aria-checked={checked ? "true" : "false"}
          aria-required={required ? "true" : "false"}
          aria-disabled={disabled ? "true" : "false"}
          aria-labelledby={ariaLabelledby}
          tabIndex={-1}
          role="switch"
          onChange={toggleSwitch}
          onKeyUp={handleKeyUp}
        />
        <span
          tabIndex={0}
          className={computedSwitchSliderClass}
          onKeyUp={handleKeyUp}
        ></span>
      </label>
      {label && <span className="sd-switch__label">{label}</span>}
    </div>
  );
};

export default Switch;
