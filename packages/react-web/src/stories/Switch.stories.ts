import type { Meta, StoryObj } from "@storybook/react";
// import { expect, jest } from '@storybook/jest';
import { expect, fn, userEvent, within } from "@storybook/test";
import React from "react";
import { Switch } from "components";

const meta: Meta<typeof Switch> = {
  title: "Fabric/Inputs/Switch",
  component: Switch,
  argTypes: {
    checked: {
      control: "boolean",
      description: "Whether the switch is checked",
    },
    disabled: {
      control: "boolean",
      description: "Whether the switch is disabled",
    },
    label: { control: "text", description: "Switch label" },
    value: { control: "text", description: "Value of the switch" },
    required: {
      control: "boolean",
      description: "Whether the switch is required",
    },
    ariaLabelledby: {
      control: "text",
      description: "ID of the element that serves as the label for the switch",
    },
    reversed: {
      control: "boolean",
      description: "Whether the switch is reversed",
    },
    invalid: {
      control: "select",
      options: [true, false, null],
      description: "Whether the switch is invalid",
    },
  },
  args: {
    checked: false,
    disabled: false,
    label: "Switch",
    reversed: false,
    invalid: null,
    value: "switch",
  },
  parameters: {
    docs: {
      description: {
        component: `Switch is a component used for allowing a user to make a binary choice.`,
      },
    },
    design: {
      type: "figma",
      url: "https://www.figma.com/file/cC06tdT15MlHYENcm4ImrV/eSentire---Design-System-(Bootstrap)?type=design&node-id=109-35659&mode=dev",
    },
  },
};

export default meta;
type Story = StoryObj<typeof Switch>;

export const Default: Story = {
  args: {
    onChange: fn(),
    onUpdateChecked: fn(),
    checked: false,
  },
  play: async ({ args, canvasElement }) => {
    const canvas = within(canvasElement);
    await expect(canvas.getByText(args.label!)).toBeTruthy();
    const switchBtn = canvas.getByRole("switch");
    await userEvent.click(switchBtn);
    await expect(args.onChange).toHaveBeenCalledWith(true, args.value);
    await expect(args.onUpdateChecked).toHaveBeenCalledWith(true, args.value);

    // reset
    await userEvent.click(switchBtn);
  },
};

export const SwitchOn: Story = {
  args: {
    ...Default.args,
    checked: true,
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    await expect(canvas.getByRole("switch")).toBeChecked();
  },
};

export const SwitchOnValid: Story = {
  args: {
    ...Default.args,
    checked: true,
    invalid: false,
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    await expect(canvas.getByRole("switch")).toHaveClass(
      "sd-switch__slider--valid"
    );
  },
};

export const SwitchOnInvalid: Story = {
  args: {
    ...Default.args,
    checked: true,
    invalid: true,
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    await expect(canvas.getByRole("switch")).toHaveClass(
      "sd-switch__slider--invalid"
    );
  },
};

export const SwitchOnReversed: Story = {
  args: {
    ...Default.args,
    checked: true,
    reversed: true,
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    await expect(canvas.getByRole("switch")).toHaveClass("sd-switch__reversed");
  },
};

export const SwitchOnReversedValid: Story = {
  args: {
    ...Default.args,
    checked: true,
    reversed: true,
    invalid: false,
  },
};

export const SwitchOnReversedInvalid: Story = {
  args: {
    ...Default.args,
    checked: true,
    reversed: true,
    invalid: true,
  },
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    disabled: true,
    label: "Disabled",
  },
  play: async ({ args, canvasElement }) => {
    const canvas = within(canvasElement);
    const switchBtn = canvas.getByRole("switch");
    await userEvent.click(switchBtn);
    await expect(args.onChange).not.toHaveBeenCalled();
    await expect(args.onUpdateChecked).not.toHaveBeenCalled();
  },
};

export const DisabledAndReversed: Story = {
  args: {
    ...Default.args,
    disabled: true,
    reversed: true,
    label: "Disabled and Reversed",
  },
};

export const DisabledAndSwitchOn: Story = {
  args: {
    ...Default.args,
    checked: true,
    disabled: true,
    label: "Disabled and Switch On",
  },
};
