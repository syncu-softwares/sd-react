
const KEY_ENTER='Enter';
const KEY_NUM_ENTER='NumpadEnter';
const EVENT_TYPE_CLICK='click'
const ESC_KEY_CODE = "Escape"

export{
    KEY_ENTER,
    KEY_NUM_ENTER,
    EVENT_TYPE_CLICK,
    ESC_KEY_CODE
}
