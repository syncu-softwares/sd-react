import React from "react";
import { Meta, StoryObj } from "@storybook/react";
import { expect } from "@storybook/test";
import { DsButtonGroup, DsButton } from "@/index";

const meta: Meta<typeof DsButtonGroup> = {
  title: "Components/Button/Group",
  component: DsButtonGroup,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
  argTypes: {
    size: {
      control: "select",
      options: ["sm", "md", "lg", "xl"],
    },
    position: {
      control: "select",
      options: ["start", "end"],
    },
  },
  args: {
    size: "md",
    gap: false,
    position: "start",
  },
};

export default meta;

type Story = StoryObj<typeof meta>;

export const Default: Story = {
  render: (args) => (
    <DsButtonGroup {...args}>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
    </DsButtonGroup>
  ),
  play: async ({ canvasElement }) => {
    const group = canvasElement.querySelector(".es-btn-group");
    await expect(group).toBeInTheDocument();
    await expect(group?.children.length).toEqual(3);
  },
};

export const AsActive: Story = {
  render: (args) => (
    <DsButtonGroup {...args}>
      <DsButton variant="primary" active outline size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" outline size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" outline size={args.size}>
        Click me
      </DsButton>
    </DsButtonGroup>
  ),
  play: async ({ canvasElement }) => {
    const group = canvasElement.querySelector(".es-btn-group");
    await expect(group).toBeInTheDocument();
    await expect(group?.children.length).toEqual(3);
    await expect(group?.children[0]).toHaveClass("active");
  },
};

export const WithGap: Story = {
  args: {
    gap: true,
  },
  render: (args) => (
    <DsButtonGroup {...args}>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
    </DsButtonGroup>
  ),
  play: async ({ canvasElement }) => {
    const group = canvasElement.querySelector(".es-btn-group-gap");
    await expect(group).toBeInTheDocument();
  },
};

export const Vertical: Story = {
  args: {
    horizontal: false,
  },
  render: (args) => (
    <DsButtonGroup {...args}>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
    </DsButtonGroup>
  ),
  play: async ({ canvasElement }) => {
    const group = canvasElement.querySelector(".es-btn-group-vertical");
    await expect(group).toBeInTheDocument();
  },
};

export const VerticalWithGap: Story = {
  args: {
    horizontal: false,
    gap: true,
  },
  render: (args) => (
    <DsButtonGroup {...args}>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
      <DsButton variant="primary" size={args.size}>
        Click me
      </DsButton>
    </DsButtonGroup>
  ),
  play: async ({ canvasElement }) => {
    const group = canvasElement.querySelector(
      ".es-btn-group-gap.es-btn-group-vertical"
    );
    await expect(group).toBeInTheDocument();
  },
};
