import type { Meta, StoryObj } from "@storybook/react";
import { DsButton, IconList } from "@/index";
import { expect, userEvent, within, fn } from "@storybook/test";
import React, { useState } from "react";
import { Variant } from "@/types";

const meta = {
  title: "Components/Button/Button",
  component: DsButton,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
  argTypes: {
    variant: {
      type: "string",
      control: "select",
      options: [
        "primary",
        "secondary",
        "info",
        "success",
        "danger",
        "warning",
        "neutral",
        "dark",
        "light",
        "link",
        "neutral",
      ],
    },
    size: {
      type: "string",
      control: "select",
      options: ["sm", "md", "lg", "xl"],
    },
    icon: {
      type: "string",
      control: "select",
      options: Object.keys(IconList),
    },
    iconAfter: {
      type: "string",
      control: "select",
      options: Object.keys(IconList),
    }
  },
  args: {
    variant: "primary",
    size: "md",
    outline: false,
    circular: false,
    disabled: false,
    active: false,
    dashline: false,
    onClick: fn(),
    children: "Button",
  },
} satisfies Meta<typeof DsButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  async play({ args, canvasElement }) {
    const canvas = within(canvasElement);
    const button = canvas.getByText("Button");
    await expect(button).toBeTruthy();
    await userEvent.click(button);
    await expect(args.onClick).toBeCalled();
  },
};

export const IconButton: Story = {
  args: {
    icon: "file-plus",
  },
  async play({ canvasElement }) {
    const icon = canvasElement.querySelector(".ds-icon");
    await expect(icon).toBeInTheDocument();
  },
};

export const DashLineButton: Story = {
  args: {
    icon: "file-plus",
    dashline: true,
  },
  play: async ({ canvasElement }) => {
    const dashline = canvasElement.querySelector(".ds-btn-dashline");
    await expect(dashline).toBeInTheDocument();
  },
};

export const AllInOne: Story = {
  render: (args) => {
    const allType: Variant[] = [
      "primary",
      "secondary",
      "info",
      "success",
      "danger",
      "warning",
      "neutral",
      "light",
      "dark",
    ];
    return (
      <div className="ds-d-flex ds-flex-column ds-gap-2">
        <div>Solid:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton key={variant} {...args} variant={variant} outline={false}>
              Button
            </DsButton>
          ))}
        </div>
        <div>Outlined:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton key={variant} {...args} variant={variant} outline={true}>
              Button
            </DsButton>
          ))}
        </div>
        <div>Dashline:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton key={variant} {...args} variant={variant} dashline={true}>
              Button
            </DsButton>
          ))}
        </div>
        <div>Text/Link:</div>
        <div className="ds-d-flex ds-gap-2">
          <DsButton {...args} variant="link" outline={false}>
            Button
          </DsButton>
        </div>
        <div>Disabled Solid:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton
              key={variant}
              {...args}
              variant={variant}
              outline={false}
              disabled={true}
            >
              Button
            </DsButton>
          ))}
        </div>
        <div>Disabled Outlined:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton
              key={variant}
              {...args}
              variant={variant}
              outline={true}
              disabled={true}
            >
              Button
            </DsButton>
          ))}
        </div>
        <div>Icon Solid:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton
              key={variant}
              {...args}
              icon="file-plus"
              variant={variant}
            />
          ))}
        </div>
        <div>Icon Outlined:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton
              key={variant}
              {...args}
              icon="file-plus"
              variant={variant}
              outline={true}
            />
          ))}
        </div>
        <div>Icon Dashline:</div>
        <div className="ds-d-flex ds-gap-2">
          {allType.map((variant) => (
            <DsButton
              key={variant}
              {...args}
              icon="file-plus"
              variant={variant}
              dashline={true}
            />
          ))}
        </div>
      </div>
    );
  },
};
