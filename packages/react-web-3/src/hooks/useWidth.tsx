import React, {
  ReactElement,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";

export const WidthContext = React.createContext<{
  width: number;
  isMobile: boolean;
}>({ width: 1200, isMobile: false });

export function WidthProvider({ children }: { children: ReactElement }) {
  const [width, setWidth] = useState<number>(window.innerWidth);
  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth);
  };
  useEffect(() => {
    window.addEventListener("resize", handleWindowSizeChange);
    return () => {
      window.removeEventListener("resize", handleWindowSizeChange);
    };
  }, []);

  const isMobile = useMemo(() => width <= 768, [width]);

  return (
    <WidthContext.Provider
      value={{
        width,
        isMobile,
      }}
    >
      {" "}
      {children}
    </WidthContext.Provider>
  );
}

export const useWidth = () => {
  const data = useContext(WidthContext);
  if (!data) {
    throw new Error("useWidth must be used within WidthProvider");
  }
  return data;
};
