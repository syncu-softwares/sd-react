import React, { useRef, useEffect, Ref, RefObject, ReactNode } from "react";

export function OutsideAlerter({ children, hide }: { children: ReactNode, hide: () => void }) {
  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef, hide);
  return (
    <div ref={wrapperRef}> {children} </div>
  )
}

export function useOutsideAlerter(ref: RefObject<HTMLDivElement>, hide: () => void) {
  useEffect(() => {
    function handleClickOutside(event: any) {
      if (ref.current && !ref.current.contains(event.target)) {
        hide();
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
}

