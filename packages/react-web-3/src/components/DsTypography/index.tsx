import React from "react";
import "./DsTypography.scss";

type HTMLTag =
  | "div"
  | "span"
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "h5"
  | "h6"
  | "p"
  | "caption";

export type DsTypographyProps = {
  /**
   * The HTML tag to use for the component.
   * Ex. div, span, h1 to h6, caption, etc.
   * @default 'div'
   */
  as?: HTMLTag;
  is?:
    | "title"
    | "body"
    | "body-small"
    | "body-regular"
    | "body-large"
    | "body-x-large"
    | "button-sm"
    | "button-md"
    | "button-lg"
    | "button-xl"
    | "badge"
    | "badge-bold"
    | "link"
    | "link-emphasis"
    | "link-small"
    | "link-small-emphasis"
    | "link-large"
    | "link-large-emphasis"
    | "link-bold"
    | "link-bold-emphasis"
    | "link-small-bold"
    | "link-small-bold-emphasis"
    | "link-large-bold"
    | "link-large-bold-emphasis"
    | "graph"
    | "graph-bold"
    | "accordion-header"
    | "modal-title"
    | "popover-title"
    | "toast-title"
    | "table-header-md"
    | "table-header-sm";
  children?: React.ReactNode;
  className?: string;
} & React.HTMLAttributes<HTMLElement>;

export const DsTypography: React.FC<DsTypographyProps> = ({
  as: Tag = "div",
  is,
  children,
  className = "",
  ...rest
}) => {
  return (
    <Tag
      className={[
        "ds-typography",
        Tag !== "div" ? `ds-typography--${Tag}` : "",
        is ? `ds-typography--${is}` : "",
        className,
      ]
        .filter(Boolean)
        .join(" ")}
      {...rest}
    >
      {children}
    </Tag>
  );
};
