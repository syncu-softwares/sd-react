import React, { UIEvent, useEffect, useState } from "react";
import IconList from "./IconList.json";
import "./DsIcon.scss";

// Helper function to dynamically import SVGs as strings
const importIcons = async (name: keyof typeof IconList) => {
  try {
    const iconsRaw = import.meta.glob("./list/**.svg", {
      query: "raw",
      import: "default",
    });
    if (IconList[name]) {
      const iconModule = await iconsRaw[`./list/${name}.svg`]();
      return iconModule;
    } else {
      console.info("Icon name not supported!");
      return null;
    }
  } catch (error) {
    console.error("Failed to get SVG:", error);
    return null;
  }
};

export { IconList };

export const DsIcon = ({
  size = "md",
  name,
  className = "",
  isAction = false,
  ariaLabel,
  onClick,
}: {
  /**
   * Icon size, set default size using font-size with style for custom size
   */
  size?: "sm" | "md" | "lg" | "xl";
  /**
   * Name of the Icon
   */
  name?: keyof typeof IconList;
  className?: string;
  isAction?: boolean;
  ariaLabel?: string;
  onClick?: (e: UIEvent) => void;
}) => {
  const [svgContent, setSvgContent] = useState("");

  useEffect(() => {
    const loadIcon = async () => {
      if (name) {
        const svg = await importIcons(name);
        svg && setSvgContent(svg as string);
      }
    };
    loadIcon();
  }, [name]);

  return (
    <i
      className={[
        "ds-icon",
        `ds-icon-size-${size}`,
        isAction ? "ds-icon--action" : "",
        className,
      ].join(" ")}
      tabIndex={isAction ? 0 : undefined}
      aria-label={ariaLabel || name}
      onClick={onClick}
      onKeyDown={(e) => {
        if (e.key === "Enter" || e.key === " ") {
          e.preventDefault();
          onClick && onClick(e);
        }
      }}
      dangerouslySetInnerHTML={{ __html: svgContent }}
    />
  );
};
