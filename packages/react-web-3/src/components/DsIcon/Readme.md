## Icons

### Icon Update/add Process

1. move all icon from sub-directory to one folder
   ```
   find ./assets -name "*.svg" -exec mv {} ./list \;
   ```
2. execute `python createList.py` in terminal to update `IconList.json` file with new icon
