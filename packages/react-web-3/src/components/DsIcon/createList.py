# To convert all svg files to JSON file with name for simplicity
import os
import json

# Replace with your targeted directory
target_dir = "./list"

# Store all SVG files in a list
svg_files = {}
for root, dirs, files in os.walk(target_dir):
    for file in files:
        if file.endswith('.svg'):
            file_without_ext = os.path.splitext(file)[0]
            svg_files[file_without_ext] = True

# Write the list to a JSON file
with open('IconList.json', 'w') as f:
    json.dump(svg_files, f, indent=2, sort_keys=True)