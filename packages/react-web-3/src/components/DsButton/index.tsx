import React, {
  MouseEventHandler,
  ReactNode,
  useContext,
  useMemo,
} from "react";
import { DsIcon, IconList } from "..";
import { DsTypography } from "..";
import { ButtonContext } from "@/contexts";
import "./DsButton.scss";
import { Variant } from "@/types";

export type DsButtonProps = {
  /*
   * Variant of the button
   */
  variant?: Variant;
  /**
   * outline variant of the button
   */
  outline?: boolean;
  /**
   * Name of the icon (Prepend)
   */
  icon?: keyof typeof IconList;
  /**
   * Name of the icon (Append)
   */
  iconAfter?: keyof typeof IconList;
  /**
   * With Circular edge
   */
  circular?: boolean;
  /**
   * Disabled property of the button
   */
  disabled?: boolean;
  /**
   * Active state of button
   */
  active?: boolean;
  /**
   * Size of a button
   */
  size?: "sm" | "md" | "lg" | "xl";
  /**
   * Dashline variant of the button
   */
  dashline?: boolean;
  /**
   * Children of button
   */
  children: ReactNode;
  /**
   * Click Event handler
   */
  onClick?: MouseEventHandler<HTMLButtonElement>;
};

export const DsButton: React.FC<DsButtonProps> = ({
  variant = "primary",
  outline = false,
  icon,
  iconAfter,
  circular,
  disabled = false,
  active,
  size = "md",
  dashline = false,
  children,
  onClick,
}) => {
  const parentData = useContext(ButtonContext);

  const computedSize = size || parentData?.size || "md";

  const computedButtonClass = useMemo(() => {
    return [
      "ds-btn",
      `ds-btn-${variant}`,
      active && "active",
      `ds-btn-${computedSize}`,
      outline && "ds-btn-outlined",
      dashline && "ds-btn-dashline",
      circular && "ds-btn--circle",
      disabled && "disabled",
    ]
      .filter(Boolean)
      .join(" ");
  }, [variant, active, computedSize, outline, dashline, circular, disabled]);

  const handleButtonClick: MouseEventHandler<HTMLButtonElement> = (event) => {
    if (!disabled && onClick) {
      event.stopPropagation();
      onClick(event);
    }
  };

  return (
    <button
      className={computedButtonClass}
      disabled={disabled}
      onClick={handleButtonClick}
    >
      {icon && <DsIcon name={icon} size={computedSize} />}
      {children && (
        <DsTypography is={`button-${computedSize}`}>{children}</DsTypography>
      )}
      {iconAfter && <DsIcon name={iconAfter} size={computedSize} />}
    </button>
  );
};
