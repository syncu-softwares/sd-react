import React, { FC, ReactNode, useMemo } from "react";
import { ButtonContext } from "@/contexts";

export type DsButtonGroupProps = {
  /**
   * The size of the button
   */
  size?: "sm" | "md" | "lg";
  /**
   * Whether to add a gap between buttons
   */
  gap?: boolean;
  /**
   * Whether to display buttons horizontally
   */
  horizontal?: boolean;
  /**
   * The position of the button group
   */
  position?: "start" | "end";
  /**
   * Children of Button Group
   */
  children?: ReactNode;
};

export const DsButtonGroup: FC<DsButtonGroupProps> = ({
  size = "md",
  gap = false,
  horizontal = true,
  position = "start",
  children,
}) => {
  const computedButtonGroupClass = useMemo(() => {
    return [
      "es-btn-group",
      gap && "es-btn-group-gap",
      !horizontal && "es-btn-group-vertical",
      position === "end" && "es-btn-group-end",
    ]
      .filter(Boolean)
      .join(" ");
  }, [gap, horizontal, position]);

  return (
    <ButtonContext.Provider value={{ size }}>
      <div className={computedButtonGroupClass}>{children}</div>
    </ButtonContext.Provider>
  );
};
