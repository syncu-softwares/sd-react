export type Variant =
  | "primary"
  | "secondary"
  | "info"
  | "success"
  | "danger"
  | "warning"
  | "light"
  | "dark"
  | "link"
  | "neutral";
