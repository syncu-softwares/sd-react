
## Installation

The best way to consume SD-React is via the npm package which you can install with `npm` (or `yarn` if you prefer).

```bash
npm i @syncu-softwares/sd-react
```

### What you'll need

- [Node.js](https://nodejs.org/en/download/) version 16.14 or above:
  - When installing Node.js, you are recommended to check all checkboxes related to dependencies.

## Importing Components

You should import components like:

```jsx
import { Button } from 'react-bootstrap';
```

## Usage

```tsx
import { Popover } from "@syncu-softwares/sd-react";

function PopoverExample() {
  return (
    <>
      <Popover
        trigger="mouseenter focusin"
        content={<>Im a Tooltip</>}
        placement="auto"
      >
        <span>Hover me</span>
      </Popover>

      <Popover
        trigger="click"
        content={<>Im a Popover</>}
        placement="auto"
      >
        <span>Click me</span>
      </Popover>
    </>
  );
}
```

## Documentation

Work in progress !!