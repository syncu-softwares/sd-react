
## Installation

The best way to consume SD-React is via the npm package which you can install with `npm` (or `yarn` if you prefer).

```bash
npm i @syncu-softwares/sd-react-consumables
```

### What you'll need

- [Node.js](https://nodejs.org/en/download/) version 16.14 or above:
  - When installing Node.js, you are recommended to check all checkboxes related to dependencies.

