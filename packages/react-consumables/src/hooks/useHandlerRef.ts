import { MutableRefObject, useEffect, useRef } from "react";

export default function useHandlerRef<T>(handler: T): MutableRefObject<T> {
  const handlerRef = useRef<T>(handler);
  useEffect(() => {
    handlerRef.current = handler;
  }, [handler]);
  return handlerRef;
}
