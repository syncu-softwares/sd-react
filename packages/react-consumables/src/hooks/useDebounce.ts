import React, { useEffect, useState } from "react";

/**
 * Debounce changes in a value (uses setTimeout internally)
 * @param value The value that is rapidly changing
 * @param delay How long to wait in milliseconds
 * @returns The same value that was passed in, but delayed
 */
export default function useDebounce<T>(value: T, delay = 300): T {
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);
    return () => clearTimeout(handler);
  }, [value, delay]);
  return debouncedValue;
}
