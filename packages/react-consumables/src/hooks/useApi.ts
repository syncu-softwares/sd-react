import {
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useState,
} from "react";
import useDebounce from "./useDebounce";
import useHandlerRef from "./useHandlerRef";

type ErrorWithStatus = {
  status: number;
};

function errorHasStatus(x: unknown): x is ErrorWithStatus {
  return (x as ErrorWithStatus).status !== undefined;
}

export type UseApiParam<DataType, ArgsType extends unknown[]> = (
  ...args: ArgsType
) => Promise<DataType>;

export type UseApiOptions<DataType, ArgsType> = {
  makeCallIf?: unknown;
  initialResult?: DataType;
  defaultArgs?: ArgsType;
  onSuccess?: (data: DataType | null) => void;
  onError?: (errorObj: unknown) => boolean | void;
  debounce?: number;
};

export type UseApiReturn<DataType, ArgsType extends unknown[]> = {
  apiCall: (
    ...args: ArgsType | [Record<string, unknown>] | []
  ) => Promise<DataType | null>;
  isLoading: boolean;
  isError: boolean;
  errorStatus: string | number | null;
  errorObj: unknown;
  result: DataType | DataType[] | null;
  setResult: Dispatch<SetStateAction<DataType | DataType[] | null>>;
  reload: () => void;
  reset: () => void;
};

export default function useApi<
  DataType = any,
  ArgsType extends unknown[] = any[]
>(
  param: UseApiParam<DataType | DataType[], ArgsType>,
  options: UseApiOptions<DataType | DataType[], ArgsType> = {}
): UseApiReturn<DataType, ArgsType> {
  const { makeCallIf, defaultArgs, initialResult, onSuccess, onError } =
    options;
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<unknown>(null);
  const [result, setResult] = useState<DataType | DataType[] | null>(
    initialResult ?? null
  );
  const [trigger, reload] = useReducer((state) => !state, false);

  const apiCallInternal = useCallback(
    async (
      ...args: ArgsType | [Record<string, unknown>] | []
    ): Promise<DataType | null> => {
      if (args.length === 0 && defaultArgs) args = defaultArgs;
      setError(null);
      setIsLoading(true);

      try {
        let resp;
        if (
          param.prototype &&
          param.prototype.constructor.length > args.length
        ) {
          throw new Error(
            "Attempted to make API call with no args or default args"
          );
        }
        resp = await param(...(args as ArgsType));
        setResult(resp);
        try {
          onSuccess?.(resp);
        } catch (error: unknown) {
          //Don't bury or escalate errors in the onSuccess callback
          console.error(error);
        }
      } catch (error: unknown) {
        console.log(error);
        setError(error);
        onError?.(error);
        throw error;
      } finally {
        setIsLoading(false);
      }
      return null;
    },
    [param, onError, onSuccess, defaultArgs]
  );

  const makeCallIfCalculated = useMemo(
    () => (makeCallIf === undefined ? defaultArgs !== undefined : !!makeCallIf),
    [makeCallIf, defaultArgs]
  );

  const defaultArgsJSON = useDebounce(
    JSON.stringify(defaultArgs),
    options.debounce ?? 0
  );

  useEffect(() => {
    if (makeCallIfCalculated) {
      apiCallInternal();
    }
    //Stringfy-ing defaultArgs to prevent re-making the call every render if args don't actually change
  }, [makeCallIfCalculated, trigger, defaultArgsJSON]);

  const errorStatus = useMemo(() => {
    if (error) {
      if (typeof error === "string") return error;
      else if (errorHasStatus(error)) return error.status;
      else if (error instanceof Error) return error.message;
    }
    return null;
  }, [error]);

  //Return a stable callback, so that using apiCall in a useEffect downstream doesn't cause re-render loops
  const apiCallRef = useHandlerRef(apiCallInternal);
  const apiCallStable = useCallback(
    (...args: ArgsType | [Record<string, unknown>] | []) =>
      apiCallRef.current?.(...args),
    [apiCallRef]
  );

  const reset = useCallback(() => {
    setIsLoading(false);
    setError(null);
    setResult(initialResult ?? null);
    // Ignoring initialResult changes, as it is usually unchanging anyways and reset needs to be stable
  }, []);

  return {
    apiCall: apiCallStable,
    isLoading,
    isError: !!error,
    errorObj: error,
    errorStatus,
    result,
    setResult,
    reload,
    reset,
  };
}
