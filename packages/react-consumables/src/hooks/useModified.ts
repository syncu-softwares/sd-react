import { isEqual } from "lodash";
import { useMemo, useState } from "react";

export default function useModified<T>(value: T): [boolean, ()=>void] {
    const [initialValue, setInitialValue] = useState(value)
    const modified = useMemo(()=> !isEqual(initialValue, value), [value, initialValue])
    const resetInitial = () => setInitialValue(value);
  return [modified, resetInitial];
}
